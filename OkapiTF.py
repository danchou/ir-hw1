#!/usr/bin/python

from elasticsearch import Elasticsearch
import re
import sys
import operator

TF = re.compile('.*termFreq=(.*?)\'.*')

class Foo:

    def __init__(self):
        self.es = Elasticsearch()
        self.doc = self.es.count(index='ap_dataset')['count']

        self.scores = [0.0] * self.doc
        self.docLen = [1.0] * self.doc
        self.avgLen = 0.0 
        self.initDocLen()

    def initDocLen(self):
        self.initDocLenFromCache()
        self.avgLen = sum(self.docLen) / float(len(self.docLen))
        #print self.avgLen

    def initDocLenFromCache(self):
        idx = 0
        with open('LenCache') as f:
            for line in f:
                self.docLen[idx] = float(line)
                idx += 1

        assert idx == self.doc


    def initDocLenFromRest(self):
        for idx in range(self.doc):
            self.docLen[idx] = float(self.es.get(index='ap_dataset', id=idx,
                doc_type='document', fields=['len'])['fields']['len'][0])


    def query(self, query, qno, rev=True):
        terms = query.split()
        self.scores = [0.0] * self.doc 
        for term in terms:
            #print "###: term: ", term
            self.termQuery(term)
       
        top100 = sorted(range(self.doc), key = lambda i : self.scores[i],
                reverse=rev)[0:100]

        # change here
        #top100 = sorted( self.scores.items(), key=operator.itemgetter(1),
        #        reverse=rev )[0:100]

        rank = 1
        for idx in top100:
            self.getDocnoById(idx, qno, rank)
            rank += 1


    def getDocnoById(self, idx, qno, rank):
        docno = self.es.get(index='ap_dataset', id=idx, doc_type='document',
                fields=['docno'])['fields']['docno'][0].strip()
        if self.scores[idx] >= 1e-8:
            print "{0:d} Q0 {1:s} {2:d} {3:.4f} Exp" \
                .format(qno, docno, rank, self.scores[idx])


    def termQuery(self, term):
        resp = self.es.search(index='ap_dataset', doc_type='document'
                ,body={'query': {
                        'match': { 
                            'text' : {
                                'query' : term
                             }
                          }
                        },
                        'explain' : 'true',
                        'size' : 85000
                      })
        
        if resp['hits']['total'] == u'0':
            return

        #print len(resp['hits']['hits'])
        for hit in resp['hits']['hits']:
            idx = int(hit['_id'])
            #docno = hit['_source']['docno'].strip()
            details = str(hit['_explanation']['details'])
            termFreq = float(TF.match(details).group(1))
            
            cur = termFreq / (termFreq + 0.5 + 1.5 * self.docLen[idx] / self.avgLen)

            ###print "idx:{0:d}, termFreq:{1:f}, socre:{2:f}".format(idx, termFreq, cur)
            self.scores[idx] += cur


def main():
    if len(sys.argv) != 2:
        print 'error'
        return
    
    foo = Foo()

    p = re.compile('(\d+)\.(.*)') 
    with open(sys.argv[1]) as f:
        for line in f:
            if line != '\n':
                m = p.match(line)
                qno = int(m.group(1))
                qry = m.group(2)[16:]
                foo.query(qry, qno)

def test():
    foo = Foo()
    foo.query('dramatic', 10)

if __name__ == '__main__':
    main()
