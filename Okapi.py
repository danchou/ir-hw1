#!/usr/bin/python

from StopWord import StopWord

from elasticsearch import Elasticsearch

import re

es = Elasticsearch()

def search(term):
    resp = es.search(index='ap_dataset', doc_type='document'
            ,body={'query': {
                'match': { 
                    'text' : {
                        'query' : term
                        }
                    }
                },
                'explain' : 'true'
             })

    details = resp['hits']['hits'][1]['_explanation']['details']
    txt = str(details)
    m = re.match('.*termFreq=(.*?)\'.*', txt) 
    print m.group(0)
    print m.group(1)

search('disturbing')
