#!/usr/bin/python

from OkapiTF import Foo 

import math
import sys
import re
from sets import Set

TF = re.compile('.*termFreq=(.*?)\'.*')

class Laplace(Foo):
    
    def __init__(self, V):
        Foo.__init__(self)
        self.V = V 

    def getDocnoById(self, idx, qno, rank):
        docno = self.es.get(index='ap_dataset', id=idx, doc_type='document',
                fields=['docno'])['fields']['docno'][0].strip()
        print "{0:d} Q0 {1:s} {2:d} {3:.4f} Exp" \
                .format(qno, docno, rank, self.scores[idx])

    def termQuery(self, term):
        resp = self.es.search(index='ap_dataset', doc_type='document'
                ,body={'query': {
                    'match': { 
                        'text' : {
                            'query' : term
                            }
                        }
                    },
                    'explain' : 'true',
                    'size' : 85000
                    })

        s = Set()
        if not resp['hits']['total'] == u'0':
            hits = resp['hits']['hits']
            for hit in hits:
                idx = int(hit['_id'])
                s.add(idx)
                details = str(hit['_explanation']['details'])
                tf = float(TF.match(details).group(1))

                cur = math.log( (tf + 1.0) / (self.docLen[idx] + self.V) )
                self.scores[idx] += cur

        # Add document with tf as 0!
        for idx in range(self.doc):
            if not idx in s:
                self.scores[idx] += math.log(1.0 / (self.docLen[idx]+self.V))

def main():
    if len(sys.argv) != 2:
        print 'error'
        return
    
    foo = Laplace(176982.0)

    p = re.compile('(\d+)\.(.*)') 
    with open(sys.argv[1]) as f:
        for line in f:
            if line != '\n':
                m = p.match(line)
                qno = int(m.group(1))
                qry = m.group(2)[16:]
                foo.query(qry, qno)


if __name__ == '__main__':
    main()

