
from sets import Set

class StopWord:
 
    def __init__(self):
        pass

    @staticmethod
    def getStopWords(fname):
        words = []
        with open(fname) as f:
            for line in f:
                word = line.strip()
                if word:
                    words.append(word)
        return Set(words)


if __name__ == '__main__':
    print StopWord.getStopWords('stoplist.txt')
