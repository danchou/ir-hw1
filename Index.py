#!/usr/bin/python

from elasticsearch import Elasticsearch

import sys
import os
import re

TEXT = re.compile('<TEXT>(.*?)</TEXT>')
DOCNO = re.compile('<DOCNO>(.*?)</DOCNO>')

es = Elasticsearch()


idx = 0

def parseFile(filepath):

    f = open(filepath, 'r')
    print filepath

    line = f.readline()
    buf = ''
    while "" != line:
        if line.startswith('<DOC>'):
            buf = ''
        elif line.startswith('</DOC>'):
            # send to es
            match = DOCNO.match(buf)      
            docno = match.group(1)

            text = ''
            for m in re.findall('<TEXT>(.*?)</TEXT>', buf, re.DOTALL):
                text += m
            
            textlen = len(text.split())
            doc = {
                'docno' : docno,
                'len' : textlen,
                'text' : text
            }
            global idx
            es.index(index='ap_dataset', doc_type='document', id=idx, body=doc)            
            idx = idx + 1

        else:
            buf += line

        line = f.readline()

    f.close()

def parseAll(dname):
    lsf = os.listdir(dname)

    for f in lsf:
        parseFile(f)

def main():
    parseAll('.')

if __name__=='__main__':
    main()
