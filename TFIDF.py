#!/usr/bin/python

from OkapiTF import Foo 

import math
import sys
import re

TF = re.compile('.*termFreq=(.*?)\'.*')

class TFIDF(Foo):
   
    def termQuery(self, term):
        resp = self.es.search(index='ap_dataset', doc_type='document'
                ,body={'query': {
                    'match': { 
                        'text' : {
                            'query' : term
                            }
                        }
                    },
                    'explain' : 'true',
                    'size' : 85000
                    })

        if resp['hits']['total'] == u'0':
            return

        hits = resp['hits']['hits']
        hitCount = len(hits)
        for hit in hits:
            idx = int(hit['_id'])
            details = str(hit['_explanation']['details'])
            termFreq = float(TF.match(details).group(1))

            cur = termFreq / (termFreq + 0.5 + 1.5 * self.docLen[idx] / self.avgLen) \
                        * math.log( self.doc / hitCount )
            
            self.scores[idx] += cur


def main():
    if len(sys.argv) != 2:
        print 'error'
        return
    
    foo = TFIDF()

    p = re.compile('(\d+)\.(.*)') 
    with open(sys.argv[1]) as f:
        for line in f:
            if line != '\n':
                m = p.match(line)
                qno = int(m.group(1))
                qry = m.group(2)[16:]
                foo.query(qry, qno)

if __name__ == '__main__':
    main()
