#!/usr/bin/python

from elasticsearch import Elasticsearch
from sets import Set

es = Elasticsearch()

docCount = es.count(index='ap_dataset')['count']


def termVectorMagic():
    
    s = Set()

    def getDocLength(idx):
        resp = es.termvector(index='ap_dataset', doc_type='document', id=idx) 
        if not resp['term_vectors']:
            return 0

        terms = resp['term_vectors']['text']['terms']
        count = 0
        for key, value in terms.iteritems():
            s.add(key)
            #count += value['term_freq']
        
        return count 

    def getAllLength():
        for idx in range(docCount):
            getDocLength(idx)
        print len(s)

    return getAllLength


fcn = termVectorMagic()
fcn()
