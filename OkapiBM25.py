#!/usr/bin/python

from OkapiTF import Foo 

import math
import sys
import re

TF = re.compile('.*termFreq=(.*?)\'.*')

class OkapiBM25(Foo):
   
    def __init__(self, k1, k2, b):
        Foo.__init__(self)
        self.k1 = k1
        self.k2 = k2
        self.b = b

        
    def query(self, query, qno, rev=True):
        terms = re.findall('\w+', query)
        dict = {}
        for term in terms:
            if not term in dict:
                dict[term] = 1
            else:
                dict[term] += 1

        self.scores = [0.0] * self.doc 
        
        for term in terms:
            self.termQuery(term, dict[term])
       
        top100 = sorted(range(self.doc), key = lambda i : self.scores[i],
                reverse=rev)[0:100]

        rank = 1
        for idx in top100:
            self.getDocnoById(idx, qno, rank)
            rank += 1

    def termQuery(self, term, tfq):
        resp = self.es.search(index='ap_dataset', doc_type='document'
                ,body={'query': {
                    'match': { 
                        'text' : {
                            'query' : term
                            }
                        }
                    },
                    'explain' : 'true',
                    'size' : 85000
                    })

        if resp['hits']['total'] == u'0':
            return

        hits = resp['hits']['hits']
        df = len(hits)
        for hit in hits:
            idx = int(hit['_id'])
            details = str(hit['_explanation']['details'])
            tf = float(TF.match(details).group(1))

            p0 = math.log( (self.doc + 0.5) / (df + 0.5) )
            p1 = (tf + self.k1*tf) \
                    / (tf+self.k1*(1-self.b+self.b*self.docLen[idx]/self.avgLen))
            p2 = (tfq + self.k2 * tfq) / (tfq + self.k2) 
            cur = p0 * p1 * p2

            self.scores[idx] += cur


def main():
    if len(sys.argv) != 2:
        print 'error'
        return
    
    foo = OkapiBM25(1.2, 100.0, 0.75)

    p = re.compile('(\d+)\.(.*)') 
    with open(sys.argv[1]) as f:
        for line in f:
            if line != '\n':
                m = p.match(line)
                qno = int(m.group(1))
                qry = m.group(2)[16:]
                foo.query(qry, qno)

if __name__ == '__main__':
    main()
