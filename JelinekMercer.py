#!/usr/bin/python

from Laplace import Laplace

import math
import sys
import re
from sets import Set

TF = re.compile('.*termFreq=(.*?)\'.*')

class JelineMercer(Laplace):
    

    def __init__(self, V):
        Laplace.__init__(self, V)
        self.docLenSum = float(sum(self.docLen))


    def getTfSum(self, hits, cache):
        summ = 0
        for hit in hits:
            idx = int(hit['_id'])
            tf = float(TF.match(str(hit['_explanation']['details'])).group(1))
            cache[idx] = tf
            summ += tf

        return summ

    def termQuery(self, term):
        resp = self.es.search(index='ap_dataset', doc_type='document'
                ,body={'query': {
                    'match': { 
                        'text' : {
                            'query' : term
                            }
                        }
                    },
                    'explain' : 'true',
                    'size' : 85000
                    })

        s = Set()
        tfSum = 0
        if int(resp['hits']['total']) == 0:
            return
        else:
            hits = resp['hits']['hits']
            cacheTf = {}
            tfSum = self.getTfSum(hits, cacheTf)
            #print 'term: ',term, ' tfSum: ', tfSum
            for hit in hits:
                idx = int(hit['_id'])
                s.add(idx)
                tf = cacheTf[idx]
 
                p0 = self.V * tf / self.docLen[idx]
                p1 = (1.0-self.V) * (tfSum-tf) / (self.docLenSum - self.docLen[idx])
                cur = math.log(p0 + p1)

                self.scores[idx] += cur

        # Add document with tf as 0!
        for idx in range(self.doc):
            if not idx in s:
                p0 = (1.0-self.V) * tfSum / (self.docLenSum - self.docLen[idx])
                #print 'p0: ', p0, ' tfSum: ', tfSum
                self.scores[idx] += math.log(p0)


def main():
    if len(sys.argv) != 2:
        print 'error'
        return
    
    foo = JelineMercer(0.9)

    p = re.compile('(\d+)\.(.*)') 
    with open(sys.argv[1]) as f:
        for line in f:
            if line != '\n':
                m = p.match(line)
                qno = int(m.group(1))
                qry = m.group(2)[16:]
                foo.query(qry, qno)

if __name__ == '__main__':
    main()

